import 'dart:convert';
import 'dart:io';
import 'package:ami/utils/constant.dart';
import 'package:flutter/material.dart';
import 'package:path_provider/path_provider.dart';
import 'package:ami/models/user.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

class UserProvider extends ChangeNotifier {

  User user;
  bool existCache = true;
  String response;
  insertUser(toSave) async {
    var res = await _writeUser(json.encode(toSave));
    print (res);
  }
  authenticateUser ( String email, String password) async {
    var authUser = await http.post("$apiUrl1/utilisateurs/auth", body: {'email': email, 'password': password});
    if (authUser.statusCode == 200) {
      var data = json.decode(authUser.body);

      response = 'success';
      user = new User.fromJson(data);
      SharedPreferences prefs = await SharedPreferences.getInstance();
    } else {
      response = authUser.statusCode.toString();
      //throw Exception ('Failed to access server ${authUser.statusCode}');
    }
    notifyListeners();
  }
  initFromCache() async{
    var userSaved = await _readUser();
    if(userSaved != '' || userSaved != null){
      user = new User.fromJson(json.decode(userSaved));
      response = 'cache found';
    }
    else{
      response = 'no cache';
      existCache = false;
    }
    notifyListeners();
  }
  logoutUser() async{
    final file = await _localFile;
    user = null;
    file.writeAsString('');
    notifyListeners();
  }
  Future<String> _readUser() async {
    try {
      final file = await _localFile;
      // Read the file.
      String contents = await file.readAsString();
      return contents;
    } catch (e) {
      // If encountering an error, return 0.
      return null;
    }
  }
  Future<String> get _localPath async {
    final directory = await getApplicationDocumentsDirectory();
    return directory.path;
  }

  Future<File> get _localFile async {
    final path = await _localPath;
    return File('$path/user.ami');
  }

  Future<File> _writeUser(dynamic user) async {
    final file = await _localFile;
    return file.writeAsString('$user');
  }

  bool get isAuthenticated {
    return user != null;
  }
  // ---------------------------
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.user != null) {
      data['user'] = this.user.toJson();
    }
    return data;
  }
}