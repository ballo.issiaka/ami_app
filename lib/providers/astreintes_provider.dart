import 'dart:convert';
import 'dart:io';

import 'package:ami/models/collaborateur.dart';
import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';
import 'package:path_provider/path_provider.dart';

import '../models/astreintes.dart';
import '../utils/constant.dart';

class AstreinteProvider extends ChangeNotifier {
  Astreintes astreinte;
  List<Astreintes> _listAstreintes = new List();
  bool existCache = true;
  String response;
  List<Astreintes> get listAstreintes => _listAstreintes;
  Future listOflistAstreintes()async{
    var cares = await http.get("$apiUrl2/astreintes");
    if(cares.statusCode == 200){
      var data =json.decode(cares.body);
      for (int i = 0; i < data.length; i++) {
        _listAstreintes.add(new Astreintes.fromJson(data[i]));
      }
      _listAstreintes.sort((a,b)=> (a.id > b.id)? 0 : 1);
      notifyListeners();
    }else{
      print(cares.statusCode);
    }
    print(_listAstreintes);
    return _listAstreintes;
  }
Future matchastreinteWithCollabo(Astreintes astreinte)async{
  var ast_collabo = await http.get("$apiUrl2/astreintes_collabos/astreintes/${astreinte.id}");
  if(ast_collabo.statusCode == 200){
    var secData = json.decode(ast_collabo.body);
    for(int j=0; j< secData.length; j++){
      print("${secData[j]['collaborateur_id']}");
      var getCollabo = await http.get("$apiUrl2/collaborateurs/${secData[j]['collaborateur_id']}");
      if(getCollabo.statusCode == 200){
        var collaboData = json.decode(getCollabo.body);
        print(collaboData);
        astreinte.collaborateurs.add(new Collaborateur.fromJson(collaboData));
      }
      else{
        print("collabo level ${getCollabo.statusCode}");
      }
    }
  }else{
    print("astreinte 2 level ${ast_collabo.statusCode}");
  }
  return astreinte;
}

  Future<String> _readAstreinte() async {
    try {
      final file = await _localFile;
      // Read the file.
      String contents = await file.readAsString();
      return contents;
    } catch (e) {
      // If encountering an error, return 0.
      return null;
    }
  }

  Future<String> get _localPath async {
    final directory = await getApplicationDocumentsDirectory();
    return directory.path;
  }

  Future<File> get _localFile async {
    final path = await _localPath;
    return File('$path/astreinte.ami');
  }

  Future<File> _writeAstreinte(dynamic astreinte) async {
    final file = await _localFile;
    return file.writeAsString('$astreinte');
  }
}
