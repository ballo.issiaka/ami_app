import 'dart:io';

import 'package:flutter/material.dart';
import 'package:path_provider/path_provider.dart';

import '../models/cities.dart';

class CitiesProvider extends ChangeNotifier {
  Cities citie;
  bool existCache = true;
  String response;

  Future<String> _readcitie() async {
    try {
      final file = await _localFile;
      // Read the file.
      String contents = await file.readAsString();
      return contents;
    } catch (e) {
      // If encountering an error, return 0.
      return null;
    }
  }

  Future<String> get _localPath async {
    final directory = await getApplicationDocumentsDirectory();
    return directory.path;
  }

  Future<File> get _localFile async {
    final path = await _localPath;
    return File('$path/citie.ami');
  }

  Future<File> _writecitie(dynamic citie) async {
    final file = await _localFile;
    return file.writeAsString('$citie');
  }
}
