import 'dart:io';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:ami/utils/constant.dart';
import 'package:ami/models/CareCenter.dart';
import 'package:flutter/material.dart';
import 'package:path_provider/path_provider.dart';


class CaresProvider extends ChangeNotifier {
  CareCenter careCenter;
  List<CareCenter> _listCareCenters = new List();
  bool existCache = true;
  String response;


  List<CareCenter> get listCareCenters => _listCareCenters;

  Future listOfCareCenters()async{
    var cares = await http.get("$apiUrl1/care_centers");
    if(cares.statusCode == 200){
      var data =json.decode(cares.body);
      for (int i = 0; i < data.length; i++) {
        _listCareCenters.add(new CareCenter.fromJson(data[i]));
      }
      notifyListeners();
    }else{
      print(cares.statusCode);
    }
    return _listCareCenters;
  }

  Future<String> _readcareCenter() async {
    try {
      final file = await _localFile;
      // Read the file.
      String contents = await file.readAsString();
      return contents;
    } catch (e) {
      // If encountering an error, return 0.
      return null;
    }
  }

  Future<String> get _localPath async {
    final directory = await getApplicationDocumentsDirectory();
    return directory.path;
  }

  Future<File> get _localFile async {
    final path = await _localPath;
    return File('$path/carecenter.ami');
  }

  Future<File> _writecareCenter(dynamic carecenter) async {
    final file = await _localFile;
    return file.writeAsString('$carecenter');
  }
}
