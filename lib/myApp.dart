import 'package:ami/utils/colors.dart';
import 'package:flutter/material.dart';
import 'utils/constant.dart';
import 'route/routes.dart';

class MyApp extends StatefulWidget {
  static const String routeName = '/myApp';

  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: appName,
      theme: ThemeData(
        // Define the default brightness and colors.
        //brightness: Brightness.dark,
        primaryColor: Palette.white,
      ),
      initialRoute: "/",
      routes: Router.routeArray,
    );
  }
}
