import 'package:ami/models/astreintes.dart';
import 'package:ami/providers/astreintes_provider.dart';
import 'package:ami/utils/T2Colors.dart';
import 'package:ami/utils/T2Constant.dart';
import 'package:ami/utils/T2DataGenerator.dart';
import 'package:ami/utils/T2Extension.dart';
import 'package:ami/utils/T2Images.dart';
import 'package:ami/utils/T2Strings.dart';
import 'package:ami/utils/T2Widgets.dart';
import 'package:ami/utils/colors.dart';
import 'package:ami/vue/singleAstreintes.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:intl/date_symbol_data_local.dart';
import 'package:intl/intl.dart';
import 'package:loading/indicator/ball_pulse_indicator.dart';
import 'package:loading/loading.dart';
import 'package:provider/provider.dart';

import 'model.dart';

class AstreintesList extends StatefulWidget {
  @override
  _AstreinteListState createState() => _AstreinteListState();
}

class _AstreinteListState extends State<AstreintesList> {
  int selectedPos = 1;
  List<T2ListModel> mListings;
  var astreinteProvider;
  List<Astreintes> astreintesList = [];
  List<Astreintes> allAstreintes = [];
  List<String> listLibelleAstreinte = [];
  List<String> listDateAstreinte = [];
  var scaffoldKey = new GlobalKey<ScaffoldState>();
  final TextEditingController searchController = new TextEditingController();
  bool dataLoad = false;
  @override
  void didChangeDependencies() {
    astreinteProvider = Provider.of<AstreinteProvider>(context);
    super.didChangeDependencies();
  }

  @override
  void initState() {
    super.initState();
    selectedPos = 1;
    mListings = getListData();
    iniData();
    initializeDateFormatting("fr_FR", null).then((_) {});
  }

  var formatter = new DateFormat('dd/MM/yyyy');

  iniData() async {
    if (Provider.of<AstreinteProvider>(context, listen: false)
        .listAstreintes
        .isEmpty) {
      var data = await Provider.of<AstreinteProvider>(context, listen: false)
          .listOflistAstreintes();
      astreintesList =
          Provider.of<AstreinteProvider>(context, listen: false).listAstreintes;
      allAstreintes =
          Provider.of<AstreinteProvider>(context, listen: false).listAstreintes;

      astreintesList.forEach(
          (astreinte) => listLibelleAstreinte.add(astreinte.id.toString()));
      astreintesList
          .forEach((astreinte) => listDateAstreinte.add(astreinte.dateDebut));
      dataLoad = true;
      setState(() {});
    } else {
      astreintesList =
          Provider.of<AstreinteProvider>(context, listen: false).listAstreintes;
      allAstreintes =
          Provider.of<AstreinteProvider>(context, listen: false).listAstreintes;
      astreintesList.forEach(
          (astreinte) => listLibelleAstreinte.add(astreinte.id.toString()));
      astreintesList
          .forEach((astreinte) => listDateAstreinte.add(astreinte.dateDebut));
      dataLoad = true;
    }
  }

  @override
  Widget build(BuildContext context) {
    changeStatusColor(t2White);
    var width = MediaQuery.of(context).size.width;
    changeStatusColor(t2White);
    return Scaffold(
      key: scaffoldKey,
      body: GestureDetector(
        onTap: () {
          FocusScope.of(context).requestFocus(new FocusNode());
        },
        child: SafeArea(
          child: Column(
            children: <Widget>[
              TopBar("Astreintes"),
              Padding(
                padding: const EdgeInsets.all(16),
                child: Stack(
                  alignment: Alignment.centerRight,
                  children: <Widget>[
                    TextField(
                        controller: searchController,
                        onChanged: (String value) {
                          astreintesList = allAstreintes;
                          List<String> listLibelle = listLibelleAstreinte
                              .where((i) => i.contains(value))
                              .toList();
                          List<String> listDebut = listDateAstreinte
                              .where((i) => i.contains(value.toUpperCase()))
                              .toList();
                          // print(listNomQuartier);
                          astreintesList = allAstreintes
                              .where((astreinte) =>
                                  listLibelle
                                      .contains(astreinte.id.toString()) ||
                                  listDebut.contains(astreinte.dateDebut))
                              .toList();

                          setState(() {});
                        },
                        decoration: InputDecoration(
                          filled: true,
                          fillColor: t2_white,
                          hintText: t2_lbl_search,
                          contentPadding: EdgeInsets.only(
                              left: 26.0, bottom: 8.0, top: 8.0, right: 50.0),
                          focusedBorder: OutlineInputBorder(
                            borderSide:
                                BorderSide(color: t2_view_color, width: 0.5),
                            borderRadius: BorderRadius.circular(26),
                          ),
                          enabledBorder: OutlineInputBorder(
                            borderSide:
                                BorderSide(color: t2_view_color, width: 0.5),
                            borderRadius: BorderRadius.circular(26),
                          ),
                        )),
                    GestureDetector(
                      child: Padding(
                        padding: const EdgeInsets.only(right: 16.0),
                        child:
                            SvgPicture.asset(t2_search, color: t2_colorPrimary),
                      ),
                    )
                  ],
                ),
              ),
              Expanded(
                child: (astreintesList.length == 0)
                    ? Center(
                        child: Loading(
                            indicator: BallPulseIndicator(),
                            size: 50.0,
                            color: Palette.secondary),
                      )
                    : ListView.builder(
                        scrollDirection: Axis.vertical,
                        itemCount: (astreintesList.length > 10)
                            ? 10
                            : astreintesList.length,
                        shrinkWrap: true,
                        itemBuilder: (context, index) {
                          return Padding(
                              padding: const EdgeInsets.fromLTRB(16, 0, 16, 16),
                              child: Container(
                                decoration:
                                    boxDecoration(radius: 10, showShadow: true),
                                child: InkWell(
                                  onTap: () {
                                    Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) => SingleAstreinte(
                                              astreinte:
                                                  astreintesList[index])),
                                    );
                                  },
                                  child: Card(
                                    semanticContainer: true,
                                    clipBehavior: Clip.antiAliasWithSaveLayer,
                                    child: Row(
                                      children: <Widget>[
                                        Container(
                                          width: width * 0.90,
                                          child: Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: <Widget>[
                                              SizedBox(
                                                height: 8,
                                              ),
                                              Row(
                                                mainAxisAlignment:
                                                    MainAxisAlignment
                                                        .spaceBetween,
                                                children: <Widget>[
                                                  Container(
                                                    decoration: BoxDecoration(
                                                        color: t2_colorAppeler,
                                                        borderRadius: new BorderRadius
                                                                .only(
                                                            bottomRight:
                                                                const Radius
                                                                        .circular(
                                                                    16.0),
                                                            topRight: const Radius
                                                                    .circular(
                                                                16.0))),
                                                    padding:
                                                        EdgeInsets.fromLTRB(
                                                            8, 2, 8, 2),
                                                    child: Column(
                                                      crossAxisAlignment:
                                                          CrossAxisAlignment
                                                              .start,
                                                      children: <Widget>[
                                                        text(
                                                            "Période du ${formatter.format(DateTime.parse(astreintesList[index].dateDebut).toLocal())}",
                                                            textColor:
                                                                t2_white,
                                                            fontSize:
                                                                textSizeNormal,
                                                            fontFamily:
                                                                fontMedium),
                                                        SizedBox(
                                                          height: 4,
                                                        ),
                                                      ],
                                                    ),
                                                  ),
                                                ],
                                              ),
                                              SizedBox(
                                                height: 8,
                                              )
                                            ],
                                          ),
                                        )
                                      ],
                                    ),
                                    elevation: 0,
                                    shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(10.0),
                                    ),
                                    margin: EdgeInsets.all(0),
                                  ),
                                ),
                              ));
                        }),
              )
            ],
          ),
        ),
      ),
    );
  }
}
