import 'package:ami/utils/T2Colors.dart';
import 'package:ami/utils/T2Constant.dart';
import 'package:ami/utils/T2DataGenerator.dart';
import 'package:ami/utils/T2Slider.dart';
import 'package:ami/utils/T2Widgets.dart';
import 'package:ami/vue/single_service_list.dart';
import 'package:flutter/material.dart';

import 'model.dart';

class Dashboard extends StatefulWidget {
  @override
  _DashboardState createState() => _DashboardState();
}

class _DashboardState extends State<Dashboard> {
  bool passwordVisible = false;
  bool isRemember = false;
  var currentIndexPage = 0;
  List<T2Favourite> mFavouriteList;
  List<T2Slider> mSliderList;
  @override
  void initState() {
    super.initState();
    mFavouriteList = getFavourites();
    mSliderList = getSliders();
  }
  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    width = width - 50;
    final Size cardSize = Size(width, width / 1.5);
    return Column(
      children: <Widget>[
        T2SliderWidget(),
        Expanded(
          child: Padding(
            padding: const EdgeInsets.only(left: 16, right: 16),
            child: ListView.builder(
              physics: NeverScrollableScrollPhysics(),
              scrollDirection: Axis.vertical,
              padding: EdgeInsets.symmetric(vertical: 10),
              shrinkWrap: true,
              itemBuilder: (context, position) {
                return InkWell(
                  onTap: () {
                    if(mFavouriteList[position].name == "PHARMACIES"){
                      Navigator.pushNamed(context, '/pharmacies/list');
                    }
                    if(mFavouriteList[position].name == "ASTREINTES"){
                      Navigator.pushNamed(context, '/astreintes/list');
                    }
                    if(mFavouriteList[position].name == "ANNUAIRE GS2E"){
                      Navigator.pushNamed(context, '/annuaire/page');
                    }
                  },
                  child: Row(
                    children: <Widget>[
                      Image.asset(
                        mFavouriteList[position].image,
                        width: width / 3.5,
                        height: width / 3,
                      ),
                      SizedBox(
                        width: 16,
                      ),
                      Container(
                        width: width - (width / 4) - 15,
                        height: width / 2.4,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Row(
                              mainAxisAlignment:
                              MainAxisAlignment.start,
                              children: <Widget>[
                                Text(
                                  mFavouriteList[position].name,
                                  style: TextStyle(
                                      color: t2TextColorPrimary,
                                      fontFamily: fontBold,
                                      fontWeight: FontWeight.w900,
                                      fontSize: textSizeNormal,
                                      textBaseline:
                                      TextBaseline.alphabetic),
                                ),
                              ],
                            ),
                            SizedBox(height: 8),
                            Row(
                              mainAxisAlignment:
                              MainAxisAlignment.start,
                              children: <Widget>[
                                text(mFavouriteList[position].duration,
                                    textColor: t2TextColorPrimary,
                                    fontFamily: fontBold,
                                    fontSize: textSizeSmall,
                                    maxLine: 2)
                              ],
                            ),
                            SizedBox(height: 10)
                          ],
                        ),
                      ),
                      Divider(
                        height: 15.0,
                        color: Color.fromRGBO(216, 216, 216, 1.0),
                      ),
                    ],
                  )
                );
              },
              itemCount: mFavouriteList.length,
            ),
          ),
        ),
      ],
    );
  }
}
