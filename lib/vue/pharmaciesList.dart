import 'package:ami/models/CareCenter.dart';
import 'package:ami/providers/careCenters_provider.dart';
import 'package:ami/utils/T2Colors.dart';
import 'package:ami/utils/T2Constant.dart';
import 'package:ami/utils/T2DataGenerator.dart';
import 'package:ami/utils/T2Extension.dart';
import 'package:ami/utils/T2Images.dart';
import 'package:ami/utils/T2Strings.dart';
import 'package:ami/utils/T2Widgets.dart';
import 'package:ami/utils/colors.dart';
import 'package:ami/utils/constant.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:geolocator/geolocator.dart';
import 'package:latlong/latlong.dart';
import 'package:loading/indicator/ball_pulse_indicator.dart';
import 'package:loading/loading.dart';
import 'package:provider/provider.dart';
import 'package:url_launcher/url_launcher.dart' as UrlLauncher;
import 'package:url_launcher/url_launcher.dart';

import '../utils/T2Widgets.dart';
import 'model.dart';
class PharmacieList extends StatefulWidget {
  @override
  _PharmacieListState createState() => _PharmacieListState();
}

class _PharmacieListState extends State<PharmacieList> {
  int selectedPos = 1;
  List<T2ListModel> mListings;
  List<CareCenter> careList = [];
  List<CareCenter> allCare = [];
  List<String> listNomPharmacie = [];
  List<String> listNomQuartier = [];
  var scaffoldKey = new GlobalKey<ScaffoldState>();
  final TextEditingController searchController = new TextEditingController();
  bool dataLoad = false;
  Position position = null;


  @override
  void didChangeDependencies() {

   if(!dataLoad){
     iniData();
   }

   
    super.didChangeDependencies();
  }

  @override
  void initState() {
    super.initState();
    selectedPos = 1;
    mListings = getListData();
  }


  iniData() async{
    if(Provider.of<CaresProvider>(context,listen: false).listCareCenters.isEmpty){
      var data =  await Provider.of<CaresProvider>(context,listen: false).listOfCareCenters();
      careList =  Provider.of<CaresProvider>(context,listen: false).listCareCenters;
      allCare =  Provider.of<CaresProvider>(context,listen: false).listCareCenters;
      careList.forEach((care)=>listNomPharmacie.add(care.longName));
      careList.forEach((care)=>listNomQuartier.add(care.quartier));
      dataLoad = true;
      setState(() {

      });
    }else{
      careList =  Provider.of<CaresProvider>(context,listen: false).listCareCenters;
      allCare =  Provider.of<CaresProvider>(context,listen: false).listCareCenters;
      careList.forEach((care)=>listNomPharmacie.add(care.longName));
      careList.forEach((care)=>listNomQuartier.add(care.quartier));
      dataLoad = true;
    }

    position = await Geolocator().getCurrentPosition(desiredAccuracy: LocationAccuracy.high);

  }




  @override
  Widget build(BuildContext context) {
    print(careList.length);
    changeStatusColor(t2White);
    var width = MediaQuery.of(context).size.width;
    changeStatusColor(t2White);
    return Scaffold(
      key: scaffoldKey,
      body: GestureDetector(
        onTap: (){
          FocusScope.of(context).requestFocus(new FocusNode());
        },
        child: SafeArea(
          child: Column(
            children: <Widget>[
              TopBar("Pharmacies"),
              Padding(
                padding: const EdgeInsets.all(16),
                child: Stack(
                  alignment: Alignment.centerRight,
                  children: <Widget>[
                    TextField(
                        controller: searchController,
                      onChanged:(String value){
                         careList = allCare;
                         List<String> listNom = listNomPharmacie.where((i)=>i.contains(value)).toList();
                         List<String> listQuartier = listNomQuartier.where((i)=>i.contains(value.toUpperCase())).toList();
                        // print(listNomQuartier);
                         careList = careList.where((care)=>listNom.contains(care.longName) || listQuartier.contains(care.quartier)).toList();


                        setState(() {

                        });

                      },
                        decoration: InputDecoration(
                      filled: true,
                      fillColor: t2_white,
                      hintText: t2_lbl_search,
                      contentPadding: EdgeInsets.only(
                          left: 26.0, bottom: 8.0, top: 8.0, right: 50.0),
                      focusedBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: t2_view_color, width: 0.5),
                        borderRadius: BorderRadius.circular(26),
                      ),
                      enabledBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: t2_view_color, width: 0.5),
                        borderRadius: BorderRadius.circular(26),
                      ),
                    )),
                    GestureDetector(
                      child: Padding(
                        padding: const EdgeInsets.only(right: 16.0),
                        child:
                            SvgPicture.asset(t2_search, color: t2_colorPrimary),
                      ),
                    )
                  ],
                ),
              ),
              Expanded(
                child: (careList.length == 0)?
                Center(child: Loading(indicator: BallPulseIndicator(), size: 50.0,color: Palette.secondary),)
                    :
                ListView.builder(
                    scrollDirection: Axis.vertical,
                    itemCount: careList.length,
                    shrinkWrap: true,
                    itemBuilder: (context, index) {
                      return Padding(
                          padding: const EdgeInsets.fromLTRB(16, 0, 16, 16),
                          child: Container(
                            decoration:
                            boxDecoration(radius: 10, showShadow: true),
                            child: Card(
                              semanticContainer: true,
                              clipBehavior: Clip.antiAliasWithSaveLayer,
                              child: Row(
                                children: <Widget>[
                                  Container(
                                    width: width * 0.90,
                                    child: Column(
                                      crossAxisAlignment:
                                      CrossAxisAlignment.start,
                                      children: <Widget>[
                                        SizedBox(
                                          height: 2,
                                        ),
                                        Padding(
                                          padding: const EdgeInsets.only(
                                              left: 10.0),
                                          child: Column(
                                            crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                            children: <Widget>[
                                              Text(
                                                (careList[index]
                                                    .longName !=
                                                    null)
                                                    ? careList[index]
                                                    .longName
                                                    : 'Aucun Nom',
                                                style: TextStyle(
                                                    color: Colors.black, fontSize: 20, fontWeight: FontWeight.w800),
                                              ),
                                              SizedBox(
                                                height: 4,
                                              ),
                                              text(
                                                  (careList[index]
                                                      .fixNumber !=
                                                      null)
                                                      ? careList[index]
                                                      .fixNumber
                                                      : 'Aucun numero',
                                                  fontSize: textSizeMedium),
                                              SizedBox(
                                                height: 4,
                                              ),
                                              text(
                                                  truncateText(
                                                      35,
                                                      (careList[index]
                                                          .streetAdress !=
                                                          null)
                                                          ? careList[index]
                                                          .streetAdress
                                                          : 'Aucune adresse'),
                                                  fontSize: textSizeMedium,
                                                  maxLine: 1),
                                              SizedBox(
                                                height: 4,
                                              ),
                                            ],
                                          ),
                                        ),
                                        SizedBox(
                                          height: 4,
                                        ),
                                        Row(
                                          mainAxisAlignment:
                                          MainAxisAlignment
                                              .spaceBetween,
                                          children: <Widget>[
                                            SizedBox(
                                              width: 2,
                                            ),
                                            Row(
                                              children: <Widget>[
                                                FlatButton(
                                                    onPressed: () {
                                                      UrlLauncher.launch(
                                                          "tel://${careList[index].fixNumber}");
                                                    },
                                                    color: t2_colorAppeler,
                                                    textColor:
                                                    Palette.white,
                                                    child: Text('Appeler')),
                                                SizedBox(
                                                  width: 10,
                                                ),
                                                FlatButton(
                                                    onPressed: () {
                                                      /* Navigator.push(
                                                        context,
                                                        MaterialPageRoute(
                                                            builder: (context) =>
                                                                Itineraire(
                                                                    pharmacy:
                                                                    careList[index])),
                                                      ); */
                                                      openMap(position, LatLng(double.parse(careList[index].latitude), double.parse(careList[index].longitude)));
                                                    },
                                                    color:
                                                    t2_colorPrimaryDark,
                                                    textColor:
                                                    Palette.white,
                                                    child:
                                                    Text('Itinéraire'))
                                              ],
                                            )
                                          ],
                                        ),
                                      ],
                                    ),
                                  )
                                ],
                              ),
                              elevation: 0,
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(10.0),
                              ),
                              margin: EdgeInsets.all(0),
                            ),
                          ));
                    }),
              )
            ],
          ),
        ),
      ),
    );
  }

  static openMap(Position origin, LatLng destination) async {
    String googleUrl =
        'https://www.google.com/maps/dir/?api=1&origin=${origin.latitude},${origin.longitude}' +
            '&destination=${destination.latitude},${destination.longitude}';
    if (await canLaunch(googleUrl)) {
      await launch(googleUrl);
    } else {
      throw 'Impossible d\'ouvrir la map.';
    }
  }

}
