import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_webview_plugin/flutter_webview_plugin.dart';

class WebViewPage extends StatefulWidget {
  final String url;

  WebViewPage({Key key, @required this.url}) : super(key: key);

  @override
  _WebViewPageState createState() => _WebViewPageState();
}

class _WebViewPageState extends State<WebViewPage> {

  final flutterWebViewPlugin = FlutterWebviewPlugin();

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: WebviewScaffold(
        url: widget.url,
        appBar: new AppBar(
          title: Center(child: new Text("Annuaire mobile GS2E")),
          actions: <Widget>[
            IconButton(
              icon: const Icon(Icons.refresh),
              onPressed: (){
                flutterWebViewPlugin.reload();
              },
            ),
            SizedBox(
              width: 10,
            )
          ],
        ),
      ),
    );
  }
}
