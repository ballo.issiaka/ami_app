import 'package:flutter/material.dart';

import '../utils/T2Widgets.dart';

class NotificattionPage extends StatefulWidget {
  @override
  _NotificattionPageState createState() => _NotificattionPageState();
}

class _NotificattionPageState extends State<NotificattionPage> {
  @override
  Widget build(BuildContext context) {
    return ListView(
      padding: EdgeInsets.all(8),
      children: const <Widget>[
        Text("Notifications", style: TextStyle(fontWeight: FontWeight.w700, fontSize: 30),),
        SizedBox(height: 8,),
        Card(
          child: ListTile(
            leading: Icon(Icons.notifications_active),
            title: Text('Bienvenue sur AMI'),
            subtitle:
                Text('AMI est l\'applicattion qui vous offre la liste des pharmacies affiliées (GS2E, CIE,, SODECI).'),
            trailing: Icon(Icons.more_vert),
            isThreeLine: true,
          ),
        ),
      ],
    );
  }
}
