import 'dart:async';
import 'package:ami/models/CareCenter.dart';
import 'package:ami/utils/T2Colors.dart';
import 'package:ami/utils/T2Constant.dart';
import 'package:ami/utils/T2Widgets.dart';
import 'package:ami/utils/colors.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

class Itineraire extends StatefulWidget {
  CareCenter pharmacy;
  Itineraire({this.pharmacy});
  @override
  _ItineraireState createState() => _ItineraireState();
}

class _ItineraireState extends State<Itineraire> {
  Completer<GoogleMapController> _controller = Completer();
  static CameraPosition _kGooglePlex;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _kGooglePlex = CameraPosition(
      target: LatLng(double.parse(widget.pharmacy.latitude),
          double.parse(widget.pharmacy.longitude)),
      zoom: 16,
    );
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: AppBar(
        title: Text("${widget.pharmacy.longName.toLowerCase()}"),
      ),
      body: Container(
        width: MediaQuery.of(context).size.width,
        child: Stack(
          children: <Widget>[
            GoogleMap(
              markers: Set<Marker>.of([
                Marker(
                    position: LatLng(double.parse(widget.pharmacy.latitude),
                        double.parse(widget.pharmacy.longitude)),
                    markerId: MarkerId('marker1'))
              ]),
              mapType: MapType.normal,
              initialCameraPosition: _kGooglePlex,
              onMapCreated: (GoogleMapController controller) {
                _controller.complete(controller);
              },
            ),
            Positioned(
              top: 15,
              left: 10,
              child: Container(
                margin: EdgeInsets.only(
                    left: MediaQuery.of(context).size.width * 0.08),
                width: MediaQuery.of(context).size.width * 0.80,
                height: MediaQuery.of(context).size.height * 0.20,
                decoration: BoxDecoration(
                  boxShadow: [
                    BoxShadow(
                        color: Palette.dark,
                        offset: Offset(1.0, 1.0),
                        blurRadius: 0.5)
                  ],
                  color: Palette.white,
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    text(widget.pharmacy.longName,
                        textColor: t2TextColorPrimary,
                        fontSize: textSizeNormal,
                        fontFamily: fontMedium),
                    text(
                        (widget.pharmacy.streetAdress != null)
                            ? widget.pharmacy.streetAdress
                            : '',
                        isCentered: true,
                        textColor: t2_textColorSecondary,
                        fontSize: textSizeMedium,
                        fontFamily: fontMedium),
                    text(
                        (widget.pharmacy.fixNumber != null)
                            ? widget.pharmacy.fixNumber
                            : '',
                        isCentered: true,
                        textColor: t2_textColorSecondary,
                        fontSize: textSizeMedium,
                        fontFamily: fontMedium),
                    text(
                        (widget.pharmacy.gsmNumber != null)
                            ? widget.pharmacy.gsmNumber
                            : '',
                        isCentered: true,
                        textColor: t2_textColorSecondary,
                        fontSize: textSizeMedium,
                        fontFamily: fontMedium),
                    text(
                        (widget.pharmacy.openHour != null)
                            ? 'Ouvre à ${widget.pharmacy.openHour}'
                            : 'heure d\'ouverture inconnu',
                        isCentered: true,
                        textColor: t2_textColorSecondary,
                        fontSize: textSizeMedium,
                        fontFamily: fontMedium),
                    text(
                        'Cliquer sur le pointeur pour afficher l\'itinéraire ',
                        isCentered: true,
                        textColor: Colors.red,
                        fontSize: textSizeSmall,
                        fontFamily: fontMedium),
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
