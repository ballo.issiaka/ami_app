import 'package:ami/utils/T2Colors.dart';
import 'package:ami/utils/T2DataGenerator.dart';
import 'package:ami/utils/T2Extension.dart';
import 'package:ami/utils/bottom_navigation.dart';
import 'package:ami/utils/colors_template.dart';
import 'package:ami/utils/drawer.dart';
import 'package:ami/utils/images_template.dart';
import 'package:ami/vue/dashboard.dart';
import 'package:ami/vue/model.dart';
import 'package:ami/vue/notification_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class HomePage extends StatefulWidget {
  static const String routeName = '/home';

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  bool passwordVisible = false;
  bool isRemember = false;
  var currentIndexPage = 0;
  List<T2Favourite> mFavouriteList;
  List<T2Slider> mSliderList;

  @override
  void initState() {
    super.initState();
    mFavouriteList = getFavourites();
    mSliderList = getSliders();
  }

  var currentIndex = 0;
  PageController pageController = PageController(
    initialPage: 0,
    keepPage: true,
  );

  void changePage(int index) {
    setState(() {
      currentIndex = index;
    });
  }

  void changeSldier(int index) {
    setState(() {
      currentIndexPage = index;
    });
  }
  void requestRebuild() {
    if(mounted) setState(() {});
  }
  @override
  Widget build(BuildContext context) {
    changeStatusColor(t2White);
    var width = MediaQuery.of(context).size.width;
    width = width - 50;
    final Size cardSize = Size(width, width / 1.5);
    final GlobalKey<ScaffoldState> _scaffoldKey =
        new GlobalKey<ScaffoldState>();
    return Scaffold(
      key: _scaffoldKey,
      drawer: MyDrawer(),
      body: SingleChildScrollView(
            padding: EdgeInsets.only(top: 45),
            physics: ScrollPhysics(),
            child: Container(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height,
              child: PageView(
                physics:new NeverScrollableScrollPhysics(),
                controller: pageController,
                children: <Widget>[
                  Dashboard(),
                  NotificattionPage(),

                  //T2SignIn()

                  Container(child: Center(child: Text("Page en construction", style: TextStyle(color: Colors.grey),),),)

                ],
              ),
            ),
          ),
      bottomNavigationBar: CurvedNavigationBar(
        backgroundColor: t3_app_background,
        color: t3_white,
        items: <Widget>[
          SvgPicture.asset(
            t3_ic_home,
            height: 24,
            width: 24,
            fit: BoxFit.none,
          ),
          SvgPicture.asset(
            t3_notification,
            height: 24,
            width: 24,
            fit: BoxFit.none,
          ),
          SvgPicture.asset(
            t3_ic_user,
            height: 24,
            width: 24,
            fit: BoxFit.none,
          ),
        ],
        onTap: (index) {
          //Handle button tap
          currentIndex = index;
          pageController.animateToPage(index, duration: Duration(milliseconds: 500), curve: Curves.ease);
        },
      ),
    );
  }
}
