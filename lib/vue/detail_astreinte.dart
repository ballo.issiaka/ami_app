import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';


class DetailAstreinte extends StatefulWidget {
  @override
  _DetailAstreinteState createState() => _DetailAstreinteState();
}

class _DetailAstreinteState extends State<DetailAstreinte> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Detail Astreinte"),
      ),
      body: Column(
        children: <Widget>[
          SizedBox(
            height: 20,
          ),
          Container(
            height: MediaQuery.of(context).size.height * 1/4,
            decoration: BoxDecoration(
              color: Colors.white
            ),
            child: Column(
              children: <Widget>[
                SizedBox(height: 10,),
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Row(
                      children: <Widget>[
                        Text("22 Aug",style: TextStyle(color: Colors.grey,fontSize: 12),),
                        SizedBox(width: 3,),
                        Container(
                          height: 10,
                          width: 1,
                          color: Colors.grey,
                        ),
                        SizedBox(width: 3,),
                        Text("12:00 AM",style: TextStyle(color: Colors.grey,fontSize: 12),),
                      ],
                    ),
                  ),
                ),
                Divider(
                  color: Colors.grey,
                ),
                Expanded(
                  child:Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Row(
                      children: <Widget>[
                        Icon(Icons.calendar_today,color: Colors.blue,),
                        SizedBox(width: 10,),
                        Container(
                          height: 10,
                          width: 1,
                          color: Colors.grey,
                        ),
                        SizedBox(width: 10,),
                        Container(
                          height: 50,
                          width: 50,
                          decoration: BoxDecoration(
                            border: Border.all(
                              color: Colors.blue
                            )
                          ),
                            child: Center(child: Text("Today",style: TextStyle(color: Colors.blue,fontSize: 12),))
                        ),
                        SizedBox(width: 10,),
                        Container(
                            height: 50,
                            width: 50,
                            decoration: BoxDecoration(
                                border: Border.all(
                                    color: Colors.transparent
                                )
                            ),
                            child: Center(child: Text("Demain",style: TextStyle(color: Colors.blue,fontSize: 12),))
                        ),
                        SizedBox(width: 10,),
                        Container(
                            height: 50,
                            width: 50,
                            decoration: BoxDecoration(
                                border: Border.all(
                                    color: Colors.transparent
                                )
                            ),
                            child: Center(child: Text("À la date",style: TextStyle(color: Colors.blue,fontSize: 12),))
                        ),
                      ],
                    ),
                  ),
                ),
                Divider(
                  color: Colors.grey,
                ),
                Expanded(
                  child:Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Row(
                      children: <Widget>[
                        Icon(Icons.notifications,color: Colors.blue,),
                        SizedBox(width: 10,),
                        Container(
                          height: 10,
                          width: 1,
                          color: Colors.grey,
                        ),
                        SizedBox(width: 10,),
                        Container(
                            height: 50,
                            width: 50,
                            decoration: BoxDecoration(
                                border: Border.all(
                                    color: Colors.blue
                                )
                            ),
                            child: Center(child: Text("15 min",style: TextStyle(color: Colors.blue,fontSize: 12),))
                        ),
                        SizedBox(width: 10,),
                        Container(
                            height: 50,
                            width: 50,
                            decoration: BoxDecoration(
                                border: Border.all(
                                    color: Colors.transparent
                                )
                            ),
                            child: Center(child: Text("30 min",style: TextStyle(color: Colors.blue,fontSize: 12),))
                        ),
                        SizedBox(width: 10,),
                        Container(
                            height: 50,
                            width: 50,
                            decoration: BoxDecoration(
                                border: Border.all(
                                    color: Colors.transparent
                                )
                            ),
                            child: Center(child: Text("1 Heure",style: TextStyle(color: Colors.blue,fontSize: 12),))
                        ),
                      ],
                    ),
                  ),
                ),
                SizedBox(
                  height: 10,
                )
              ],
            ),
          ),
          SizedBox(height: 5,),
          Expanded(
            child: ListView(
              children: <Widget>[
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 10),
                child: Container(
                  height: MediaQuery.of(context).size.height * 1/8,
                  color: Colors.white,
                  child: Padding(
                    padding: const EdgeInsets.symmetric(vertical: 8.0,horizontal: 8.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Column(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text("Dr. Smith Rose",style: TextStyle(color: Colors.black,fontWeight: FontWeight.bold),),
                            SizedBox(height: 2,),
                            Text("Dental Surgeon",style: TextStyle(color: Colors.grey,fontSize: 10),),
                            Row(
                              children: <Widget>[
                                Text("22 Aug",style: TextStyle(color: Colors.grey,fontSize: 9),),
                                SizedBox(width: 3,),
                                Container(
                                  height: 10,
                                  width: 1,
                                  color: Colors.grey,
                                ),
                                SizedBox(width: 3,),
                                Text("12:00 AM",style: TextStyle(color: Colors.grey,fontSize: 9),),
                              ],
                            )
                          ],
                        ),
                        Container(
                          height: 30,
                          width: 90,
                          decoration: BoxDecoration(
                              border: Border.all(
                                  color: Colors.green
                              )
                          ),
                          child: Center(
                            child: Text(
                              "SE RAPPLER",
                              style: TextStyle(
                                  color: Colors.green,
                                  fontWeight: FontWeight.bold,
                                fontSize: 11
                              ),
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                ),
              ),
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 10),
                  child: Container(
                    height: MediaQuery.of(context).size.height * 1/8,
                    color: Colors.white,
                    child: Padding(
                      padding: const EdgeInsets.symmetric(vertical: 8.0,horizontal: 8.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Column(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Text("Dr. Smith Rose",style: TextStyle(color: Colors.black,fontWeight: FontWeight.bold),),
                              SizedBox(height: 2,),
                              Text("Dental Surgeon",style: TextStyle(color: Colors.grey,fontSize: 10),),
                              Row(
                                children: <Widget>[
                                  Text("22 Aug",style: TextStyle(color: Colors.grey,fontSize: 9),),
                                  SizedBox(width: 3,),
                                  Container(
                                    height: 10,
                                    width: 1,
                                    color: Colors.grey,
                                  ),
                                  SizedBox(width: 3,),
                                  Text("12:00 AM",style: TextStyle(color: Colors.grey,fontSize: 9),),
                                ],
                              )
                            ],
                          ),
                          Container(
                            height: 30,
                            width: 90,
                            decoration: BoxDecoration(
                                border: Border.all(
                                    color: Colors.green
                                )
                            ),
                            child: Center(
                              child: Text(
                                "SE RAPPLER",
                                style: TextStyle(
                                    color: Colors.green,
                                    fontWeight: FontWeight.bold,
                                    fontSize: 11
                                ),
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                )

              ],
            ),
          )
        ],
      ),
    );
  }
}
