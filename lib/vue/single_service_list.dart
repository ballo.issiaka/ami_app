import 'package:ami/utils/T2Colors.dart';
import 'package:ami/utils/T2Constant.dart';
import 'package:ami/utils/T2DataGenerator.dart';
import 'package:ami/utils/T2Extension.dart';
import 'package:ami/utils/T2Images.dart';
import 'package:ami/utils/T2Strings.dart';
import 'package:ami/utils/T2Widgets.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

import 'model.dart';



class SingleServiceList extends StatefulWidget {
  @override
  _SingleServiceListState createState() => _SingleServiceListState();
}

class _SingleServiceListState extends State<SingleServiceList> {

  int selectedPos = 1;
  List<T2ListModel> mListings;

  @override
  void initState() {
    super.initState();
    selectedPos = 1;
    mListings = getListData();
  }

  @override
  Widget build(BuildContext context) {
    changeStatusColor(t2White);
    var width = MediaQuery.of(context).size.width;
    changeStatusColor(t2White);
    return Scaffold(
      body: SafeArea(
        child: Column(
          children: <Widget>[
            TopBar("test"),
            Padding(
              padding: const EdgeInsets.all(16),
              child: Stack(
                alignment: Alignment.centerRight,
                children: <Widget>[
                  TextField(
                      decoration: InputDecoration(
                        filled: true,
                        fillColor: t2_white,
                        hintText: t2_lbl_search,
                        contentPadding: EdgeInsets.only(
                            left: 26.0, bottom: 8.0, top: 8.0, right: 50.0),
                        focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: t2_view_color,width: 0.5),
                          borderRadius: BorderRadius.circular(26),
                        ),
                        enabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: t2_view_color,width: 0.5),
                          borderRadius: BorderRadius.circular(26),
                        ),
                      )),
                  GestureDetector(
                    child: Padding(
                      padding: const EdgeInsets.only(right: 16.0),
                      child: SvgPicture.asset(t2_search, color: t2_colorPrimary),
                    ),
                  )
                ],
              ),
            ),
            Expanded(
              child: ListView.builder(
                  scrollDirection: Axis.vertical,
                  itemCount: mListings.length,
                  shrinkWrap: true,
                  itemBuilder: (context, index) {
                    return Padding(
                        padding: const EdgeInsets.fromLTRB(16, 0, 16, 16),
                        child: Container(
                          decoration: boxDecoration(radius: 10, showShadow: true),
                          child: Card(
                            semanticContainer: true,
                            clipBehavior: Clip.antiAliasWithSaveLayer,
                            child: Row(
                              children: <Widget>[
                                Image.asset(
                                  mListings[index].icon,
                                  width: width / 3,
                                  height: width / 2.8,
                                  fit: BoxFit.fill,
                                ),
                                Container(
                                  width: width - (width / 3)-35,
                                  child:Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: <Widget>[
                                      SizedBox(height: 8,),
                                      Row(
                                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                        children: <Widget>[
                                          Container(
                                            decoration: BoxDecoration(
                                                color: index%2==0?t2_red:t2_colorPrimary,
                                                borderRadius: new BorderRadius.only(
                                                    bottomRight: const Radius.circular(16.0),
                                                    topRight: const Radius.circular(16.0))

                                            ),
                                            padding: EdgeInsets.fromLTRB(8,2,8,2),
                                            child: text(index%2==0?"New":"Popular",textColor: t2_white,fontSize: textSizeSmall),
                                          ),
                                          GestureDetector(
                                              onTap: (){

                                              },
                                              child: Icon(Icons.more_vert))
                                        ],),
                                      SizedBox(height: 8,),
                                      Padding(
                                        padding: const EdgeInsets.only(left:10.0),
                                        child: Column(
                                          crossAxisAlignment: CrossAxisAlignment.start,
                                          children: <Widget>[
                                            text(mListings[index].name,textColor: t2TextColorPrimary,fontSize: textSizeNormal,fontFamily: fontMedium),
                                            SizedBox(height: 4,),
                                            text(mListings[index].duration,fontSize: textSizeMedium),
                                            SizedBox(height: 4,),
                                            text(mListings[index].description,fontSize: textSizeMedium,maxLine: 1),
                                            SizedBox(height: 4,),
                                          ],
                                        ),
                                      )

                                    ],
                                  ),
                                )
                              ],
                            ),
                            elevation: 0,
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10.0),
                            ),
                            margin: EdgeInsets.all(0),
                          ),

                        )
                    );
                  }),
            )
          ],
        ),
      ),
    );
  }
}
