import 'package:ami/models/astreintes.dart';
import 'package:ami/providers/astreintes_provider.dart';
import 'package:ami/utils/T2Colors.dart';
import 'package:ami/utils/T2Constant.dart';
import 'package:ami/utils/T2Widgets.dart';
import 'package:ami/utils/colors.dart';
import 'package:flutter/material.dart';
import 'package:loading/indicator/ball_pulse_indicator.dart';
import 'package:loading/loading.dart';
import 'package:provider/provider.dart';
import 'package:url_launcher/url_launcher.dart';

import 'package:intl/date_symbol_data_local.dart';
import 'package:intl/intl.dart';

class SingleAstreinte extends StatefulWidget {
  Astreintes astreinte;
  SingleAstreinte({this.astreinte});
  @override
  _SingleAstreinteState createState() => _SingleAstreinteState();
}

class _SingleAstreinteState extends State<SingleAstreinte> {
  var astreinteProvider;
  @override
  void didChangeDependencies() {
    astreinteProvider = Provider.of<AstreinteProvider>(context);
    super.didChangeDependencies();
  }

  @override
  void initState() {
    super.initState();
    initializeDateFormatting("fr_FR", null).then((_) {});
  }

  var formatter = new DateFormat('dd/MM/yyyy');

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Liste des collaborateurs"),
      ),
      body: Column(
        children: <Widget>[
          SizedBox(
            height: 10,
          ),
          Container(
            height: MediaQuery.of(context).size.height * 1 / 14,
            decoration: BoxDecoration(color: shadow_color),
            child: Column(
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 10),
                  child: Row(
                    children: <Widget>[
                      SizedBox(
                        width: 10,
                      ),
                      Icon(
                        Icons.notifications,
                        color: t2_colorAppeler,
                        size: 30,
                      ),
                      SizedBox(
                        width: 10,
                      ),
                      Container(
                        height: 10,
                        width: 1,
                        color: Colors.grey,
                      ),
                      SizedBox(
                        width: 10,
                      ),
                      SizedBox(
                        width: 10,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Container(
                            decoration: BoxDecoration(
                                color: t2_colorAppeler,
                                borderRadius: new BorderRadius.only(
                                    bottomRight: const Radius.circular(16.0),
                                    topRight: const Radius.circular(16.0))),
                            padding: EdgeInsets.fromLTRB(8, 2, 8, 2),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                text(
                                    "Période du ${formatter.format(DateTime.parse(widget.astreinte.created).toLocal())}",
                                    textColor: t2_white,
                                    fontSize: textSizeNormal,
                                    fontFamily: fontMedium),
                                SizedBox(
                                  height: 4,
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
          SizedBox(
            height: 5,
          ),
          Expanded(
            child: FutureBuilder(
              builder: (context, snap) {
                if (snap.hasData) {
                  return ListView.builder(
                    scrollDirection: Axis.vertical,
                    itemCount: snap.data.collaborateurs.length,
                    shrinkWrap: true,
                    itemBuilder: (context, index) {
                      return Padding(
                        padding: const EdgeInsets.all(10),
                        child: Container(
                          height: MediaQuery.of(context).size.height * 1 / 8,
                          color: Colors.white,
                          child: Padding(
                            padding: const EdgeInsets.symmetric(
                                vertical: 8.0, horizontal: 8.0),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Column(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceEvenly,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Text(
                                      "${snap.data.collaborateurs[index].nom} ${snap.data.collaborateurs[index].prenom}",
                                      style: TextStyle(
                                          color: Colors.black,
                                          fontWeight: FontWeight.bold),
                                    ),
                                    SizedBox(
                                      height: 2,
                                    ),
                                    Text(
                                      "${snap.data.collaborateurs[index].leService}",
                                      style: TextStyle(
                                          color: Colors.grey, fontSize: 12),
                                    ),
                                    Row(
                                      children: <Widget>[
                                        Text(
                                          "${snap.data.collaborateurs[index].email}",
                                          style: TextStyle(
                                              color: Colors.grey, fontSize: 9),
                                        ),
                                        SizedBox(
                                          width: 3,
                                        ),
                                        Container(
                                          height: 10,
                                          width: 1,
                                          color: Colors.grey,
                                        ),
                                        SizedBox(
                                          width: 3,
                                        ),
                                        Text(
                                          "${snap.data.collaborateurs[index].tel}",
                                          style: TextStyle(
                                              color: Colors.grey, fontSize: 12),
                                        ),
                                      ],
                                    )
                                  ],
                                ),
                                Container(
                                  height: 20,
                                  width: 60,
                                  decoration: BoxDecoration(
                                      border: Border.all(color: Colors.green)),
                                  child: Center(
                                    child: Text(
                                      "ACTIVE",
                                      style: TextStyle(
                                          color: Colors.green,
                                          fontWeight: FontWeight.bold,
                                          fontSize: 10),
                                    ),
                                  ),
                                )
                              ],
                            ),
                          ),
                        ),
                      );
                    },
                  );
                } else {
                  return Center(
                    child: Loading(
                        indicator: BallPulseIndicator(),
                        size: 50.0,
                        color: Palette.secondary),
                  );
                }
              },
              future:
                  astreinteProvider.matchastreinteWithCollabo(widget.astreinte),
            ),
          )
        ],
      ),
    );
  }
}
