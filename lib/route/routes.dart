import 'package:ami/vue/astreintesList.dart';
import 'package:ami/vue/pharmaciesList.dart';
import 'package:ami/vue/signin.dart';

import 'package:ami/vue/splash_screen.dart';

import 'package:ami/vue/web_view_page.dart';

import 'package:flutter/material.dart';
import 'package:ami/vue/home_page.dart';

class Router {

  static var routeArray = <String, WidgetBuilder>{
    "/": (BuildContext context) => new Splash(),
    "/signin": (BuildContext context) => new T2SignIn(),
    "/pharmacies/list": (BuildContext context) => new PharmacieList(),
    "/astreintes/list": (BuildContext context) => new AstreintesList(),
    "/annuaire/page": (BuildContext context) => new WebViewPage(url: "https://s1.stvffmn.com/01/annuaire/web/app.php/",),
  };
}