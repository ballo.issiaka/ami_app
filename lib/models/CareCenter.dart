class CareCenter {
  int id;
  String shortName;
  String longName;
  String fixNumber;
  String gsmNumber;
  String latitude;
  String longitude;
  String email;
  IsGuard isGuard;
  String startDateGuard;
  String endDateGuard;
  String openHour;
  String closeHour;
  String streetAdress;
  String logoCareCenter;
  String logoCareCenterContentType;
  String imgPub;
  String imgPubContentType;
  String typeCareCenter;
  String districtId;
  String quartier;
  IsGuard active;
  int version;

  CareCenter(
      {this.id,
        this.shortName,
        this.longName,
        this.fixNumber,
        this.gsmNumber,
        this.latitude,
        this.longitude,
        this.email,
        this.isGuard,
        this.startDateGuard,
        this.endDateGuard,
        this.openHour,
        this.closeHour,
        this.streetAdress,
        this.logoCareCenter,
        this.logoCareCenterContentType,
        this.imgPub,
        this.imgPubContentType,
        this.typeCareCenter,
        this.districtId,
        this.quartier,
        this.active,
        this.version});

  CareCenter.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    shortName = json['short_name'];
    longName = json['long_name'];
    fixNumber = json['fix_number'];
    gsmNumber = json['gsm_number'];
    latitude = json['latitude'];
    longitude = json['longitude'];
    email = json['email'];
    isGuard = json['is_guard'] != null
        ? new IsGuard.fromJson(json['is_guard'])
        : null;
    startDateGuard = json['start_date_guard'];
    endDateGuard = json['end_date_guard'];
    openHour = json['open_hour'];
    closeHour = json['close_hour'];
    streetAdress = json['street_adress'];
    logoCareCenter = json['logo_care_center'];
    logoCareCenterContentType = json['logo_care_center_content_type'];
    imgPub = json['img_pub'];
    imgPubContentType = json['img_pub_content_type'];
    typeCareCenter = json['type_care_center'];
    districtId = json['district_id'];
    quartier = json['quartier'];
    active =
    json['active'] != null ? new IsGuard.fromJson(json['active']) : null;
    version = json['version'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['short_name'] = this.shortName;
    data['long_name'] = this.longName;
    data['fix_number'] = this.fixNumber;
    data['gsm_number'] = this.gsmNumber;
    data['latitude'] = this.latitude;
    data['longitude'] = this.longitude;
    data['email'] = this.email;
    if (this.isGuard != null) {
      data['is_guard'] = this.isGuard.toJson();
    }
    data['start_date_guard'] = this.startDateGuard;
    data['end_date_guard'] = this.endDateGuard;
    data['open_hour'] = this.openHour;
    data['close_hour'] = this.closeHour;
    data['street_adress'] = this.streetAdress;
    data['logo_care_center'] = this.logoCareCenter;
    data['logo_care_center_content_type'] = this.logoCareCenterContentType;
    data['img_pub'] = this.imgPub;
    data['img_pub_content_type'] = this.imgPubContentType;
    data['type_care_center'] = this.typeCareCenter;
    data['district_id'] = this.districtId;
    data['quartier'] = this.quartier;
    if (this.active != null) {
      data['active'] = this.active.toJson();
    }
    data['version'] = this.version;
    return data;
  }
}

class IsGuard {
  String type;
  List<int> data;

  IsGuard({this.type, this.data});

  IsGuard.fromJson(Map<String, dynamic> json) {
    type = json['type'];
    data = json['data'].cast<int>();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['type'] = this.type;
    data['data'] = this.data;
    return data;
  }
}