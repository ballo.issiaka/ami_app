import 'package:ami/models/collaborateur.dart';

class Astreintes {
  int id;
  String libelle;
  String dateDebut;
  String dateFin;
  String etat;
  String created;
  String collabora;
  List<Collaborateur> collaborateurs = new List();

  Astreintes(
      {this.id,
      this.libelle,
      this.dateDebut,
      this.dateFin,
      this.etat,
      this.created,
      this.collabora});

  Astreintes.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    libelle = json['libelle'];
    dateDebut = json['date_debut'];
    dateFin = json['date_fin'];
    etat = json['etat'];
    created = json['created'];
    collabora = json['collabora'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['libelle'] = this.libelle;
    data['date_debut'] = this.dateDebut;
    data['date_fin'] = this.dateFin;
    data['etat'] = this.etat;
    data['created'] = this.created;
    data['collabora'] = this.collabora;
    return data;
  }
}