import 'package:ami/models/Active.dart';

class Cities {
  int id;
  String cityName;
  Active active;
  int version;

  Cities({this.id, this.cityName, this.active, this.version});

  Cities.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    cityName = json['city_name'];
    active =
        json['active'] != null ? new Active.fromJson(json['active']) : null;
    version = json['version'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['city_name'] = this.cityName;
    if (this.active != null) {
      data['active'] = this.active.toJson();
    }
    data['version'] = this.version;
    return data;
  }
}