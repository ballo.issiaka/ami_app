class Collaborateur {
  int id;
  int servicesId;
  int posteA1Id;
  String nom;
  String prenom;
  String email;
  String tel;
  String leService;
  String tel2;
  String chef;
  int numeroOrdre;
  String active;

  Collaborateur(
      {this.id,
      this.servicesId,
      this.posteA1Id,
      this.nom,
      this.prenom,
      this.email,
      this.tel,
      this.leService,
      this.tel2,
      this.chef,
      this.numeroOrdre,
      this.active});

  Collaborateur.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    servicesId = json['services_id'];
    posteA1Id = json['poste_a1_id'];
    nom = json['nom'];
    prenom = json['prenom'];
    email = json['email'];
    tel = json['tel'];
    leService = json['le_service'];
    tel2 = json['tel2'];
    chef = json['chef'];
    numeroOrdre = json['numero_ordre'];
    active = json['active'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['services_id'] = this.servicesId;
    data['poste_a1_id'] = this.posteA1Id;
    data['nom'] = this.nom;
    data['prenom'] = this.prenom;
    data['email'] = this.email;
    data['tel'] = this.tel;
    data['le_service'] = this.leService;
    data['tel2'] = this.tel2;
    data['chef'] = this.chef;
    data['numero_ordre'] = this.numeroOrdre;
    data['active'] = this.active;
    return data;
  }
}