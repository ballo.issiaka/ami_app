import 'package:ami/models/Active.dart';

class District {
  int id;
  String districtName;
  Active active;
  int version;
  int cityId;

  District(
      {this.id, this.districtName, this.active, this.version, this.cityId});

  District.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    districtName = json['district_name'];
    active =
        json['active'] != null ? new Active.fromJson(json['active']) : null;
    version = json['version'];
    cityId = json['city_id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['district_name'] = this.districtName;
    if (this.active != null) {
      data['active'] = this.active.toJson();
    }
    data['version'] = this.version;
    data['city_id'] = this.cityId;
    return data;
  }
}