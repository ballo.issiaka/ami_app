import 'package:ami/providers/astreintes_provider.dart';
import 'package:ami/providers/careCenters_provider.dart';
import 'package:flutter/material.dart';
import 'package:ami/myApp.dart';
import 'package:provider/provider.dart';

void main() => runApp(
    MultiProvider(
        providers:[
          ChangeNotifierProvider(create: (_) => CaresProvider()),
          ChangeNotifierProvider(create: (_) => AstreinteProvider()),
        ],
        child: MyApp()
    )
);

