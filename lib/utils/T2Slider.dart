import 'package:ami/vue/model.dart';
import 'package:dots_indicator/dots_indicator.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'T2Colors.dart';
import 'T2Constant.dart';
import 'T2DataGenerator.dart';
import 'T2SliderWidget.dart';
import 'T2Widgets.dart';

class T2SliderWidget extends StatefulWidget {
  static String tag = '/T2Slider';
  @override
  T2SliderWidgetState createState() => T2SliderWidgetState();
}

class T2SliderWidgetState extends State<T2SliderWidget> {
  var currentIndexPage = 0;
  List<T2Slider> mSliderList;

  @override
  void initState() {
    super.initState();
    mSliderList = getSliders();
  }

  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    width = width - 50;
    final Size cardSize = Size(width, width / 1.8);
    return Column(
      children: <Widget>[
        InkWell(
          child: T2CarouselSlider(
            viewportFraction: 0.9,
            height: cardSize.height,
            enlargeCenterPage: true,
            scrollDirection: Axis.horizontal,
            items: mSliderList.map((slider) {
              return Builder(
                builder: (BuildContext context) {
                  return Container(
                    width: MediaQuery.of(context).size.width,
                    height: cardSize.height,
                    margin: EdgeInsets.symmetric(horizontal: 8.0),
                    child: Stack(
                      children: <Widget>[
                        ClipRRect(
                          borderRadius: new BorderRadius.circular(12.0),
                          child: Image.asset(
                            slider.image,
                            fit: BoxFit.fill,
                            width: MediaQuery.of(context).size.width,
                            height: cardSize.height,
                          ),
                        ),
                      ],
                    ),
                  );
                },
              );
            }).toList(),
            onPageChanged: (index) {
              setState(() {
                currentIndexPage = index;
              });
            },
          ),
          onTap: () {
            if (currentIndexPage == 0) {
              Navigator.pushNamed(context, '/pharmacies/list');
            }
            if (currentIndexPage == 1) {
              Navigator.pushNamed(context, '/pharmacies/list');
            }
            if (currentIndexPage == 2) {
              Navigator.pushNamed(context, '/astreintes/list');
            }
          },
        ),
        SizedBox(
          height: 16,
        ),
        DotsIndicator(
            dotsCount: mSliderList.length,
            position: currentIndexPage,
            decorator: DotsDecorator(
              size: const Size.square(5.0),
              activeSize: const Size.square(8.0),
              color: t2_view_color,
              activeColor: t2_colorPrimary,
            ))
      ],
    );
  }
}
