import 'package:flutter/material.dart';

class Palette {
  static Color primary = Color(0xffFED201);
  static Color secondary = Color(0xff5B765B);
  static Color dark = Color(0xff1B1B1B);
  static Color white = Color(0xffffffff);

  static Color danger = Color(0xffF2DEDE);
  static Color warning = Color(0xffFCF8E3);
  static Color info = Color(0xffD9EDF7);
  static Color success = Color(0xffDFF0D8);

  static Color textDanger = Color(0xffC04442);
  static Color textWarning = Color(0xff8A6D3B);
  static Color textInfo = Color(0xff3186C2);
  static Color textSuccess = Color(0xff5B765B);


}