
/*fonts*/
import 'package:flutter/widgets.dart';

const  fontRegular='Regular';
const  fontMedium='Medium';
const  fontSemibold='Semibold';
const  fontBold='Bold';
const  fontWeight = FontWeight.w800;
/* font sizes*/
const textSizeSmall=12.0;
const textSizeSmallMedium=12.5;
const textSizeSMedium=14.0;
const textSizeMedium=16.0;
const textSizeLargeMedium=18.0;
const textSizeNormal=18.0;
const textSizeLarge=24.0;
const textSizeXLarge=30.0;

const profileImage='assets/images/profile.png';