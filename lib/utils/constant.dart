const appName = "AMI";
const apiUrl1 = "http://ami.francecentral.cloudapp.azure.com:3033";
const apiUrl2 = "http://mapi-registry.westeurope.cloudapp.azure.com";
String truncateText(int cutoff, String myString) {
  return (myString.length <= cutoff)
      ? myString
      : '${myString.substring(0, cutoff)}...';
}